package com.cat.finalproject.Objects;

/**
 * Created by Exchange on 9/7/2015.
 */
public class Location {

    private int id;
    private String Latitude;
    private String Longitude;

    public Location() {
    }

    public Location(int id, String latitude, String longitude) {
        this.id = id;
        Latitude = latitude;
        Longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }
}
