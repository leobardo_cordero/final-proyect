package com.cat.finalproject.Detail;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.cat.finalproject.FabLib.codetail.fabtoolbar.FabToolbar;
import com.cat.finalproject.Objects.Station;
import com.cat.finalproject.R;
import com.cat.finalproject.Utils.Constants;
import com.google.android.gms.plus.PlusOneButton;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Exchange on 8/27/2015.
 *
 * Adapter Class for the RecyclerView
 */
public class DetailAdapter extends RecyclerView.Adapter<DetailAdapter.ViewHolder>{
    /**
     * Variables
     */
    private List<Station> items;
    private List<Station> itemsOriginal;
    private OnItemClickListener listener;
    private String docksAvailable;
    private String totalDocks;
    private String bikesAvailable;

    /**
     * Constructors
     */
    public DetailAdapter() {
    }

    public DetailAdapter(List<Station> items, Context context) {
        this.items = items;
        this.itemsOriginal = items;
        docksAvailable = context.getResources().getString(R.string.docks_available);
        totalDocks = context.getResources().getString(R.string.total_docks);
        bikesAvailable = context.getResources().getString(R.string.bikes_available);
    }


    /**
     * INTERFACES
     */

    public interface OnItemClickListener {
        void onItemClick(ViewHolder item, int position);
    }

    /**
     * METHODS
     */

    public static class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        // Fields from CardView
        @InjectView(R.id.nameStation)
        TextView nameStation;

        @InjectView(R.id.docks_available)
        TextView docks_available;

        @InjectView(R.id.bikes_available)
        TextView bikes_available;

        @InjectView(R.id.empty_docks)
        TextView empty_docks;

        private PlusOneButton mPlusOneButton;
        private DetailAdapter detAdap = null;

        /**
         * Constructor
         *
         * @param v
         * @param detAdap
         */
        public ViewHolder(View v, DetailAdapter detAdap) {
            super(v);

            //v.setOnClickListener(this);
            v.findViewById(R.id.navigate).setOnClickListener(this);
            v.findViewById(R.id.minus).setOnClickListener(this);
            mPlusOneButton = (PlusOneButton)v.findViewById(R.id.plus_one_button);

            mPlusOneButton.initialize(Constants.URL_GOOGLE_PLUS_BUTTON, Constants.ZERO_VALUE);
            this.detAdap = detAdap;

            ButterKnife.inject(this,v);
        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.navigate:
                    final OnItemClickListener listener = detAdap.getOnItemClickListener();

                    if (listener != null) {
                        listener.onItemClick(this, getAdapterPosition());
                    }

                    break;
                case R.id.like:
                    break;
//                case R.id.plus_one_button:
                    // Refresh the state of the +1 button each time the activity receives focus.
//                    mPlusOneButton.initialize("https://play.google.com/store/apps/details?id=com.airportlive", Constants.ZERO_VALUE);
//                    break;
                case R.id.minus:
//                    FabToolbar fabToolbar;
//                    fabToolbar = (FabToolbar)v.findViewById(R.id.fab_toolbar);
//                    fabToolbar.hide();
                    break;
                default:
                    Log.e(Constants.TAG_ADAPTER, Constants.NO_ID_VIEW_FOUND);
                    break;
            }
        }
    }

    /**
     *  This Class Manages the creation of new objects needed for the collection
     */

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);

        return new ViewHolder(v, this);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        int x = i+1;
        Station item = Station.getItem(x);

        if(item != null && !"".equals(item.getName())) {
            String terminalDate = item.getTerminalName() != null ? item.getTerminalName() : "";
            viewHolder.nameStation.setText(terminalDate + " " + item.getName());
            viewHolder.nameStation.setText(item.getName() != null ? item.getName() : "");

            viewHolder.empty_docks.setText(docksAvailable + item.getNbEmptyDocks());
            viewHolder.bikes_available.setText(bikesAvailable + item.getNbBikes());
            viewHolder.docks_available.setText(totalDocks + item.getNbDocks());
        } else {
            Log.e(Constants.TAG_ADAPTER, Constants.NO_STATION_ERROR + x);
        }
    }

    /**
     * FILTER METHODS
     *
     */

    public void animateTo(List<Station> models) {
        Log.i(Constants.TAG_ADAPTER, "AnimateTo size: " + models.size());
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    private void applyAndAnimateRemovals(List<Station> newModels) {
        for (int i = items.size() - 1; i >= 0; i--) {
            final Station model = items.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<Station> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final Station model = newModels.get(i);
            if (!items.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<Station> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final Station model = newModels.get(toPosition);
            final int fromPosition = items.indexOf(model);

            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }


    public Station removeItem(int position) {
        final Station model = items.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, Station model) {
        items.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final Station model = items.remove(fromPosition);
        items.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }


    public List<Station> getItemsOriginal() {
        return itemsOriginal;
    }

    /**
     * GETTERS & SETTERS
     *
     */

    @Override
    public long getItemId(int position) {
        return Station.getItem(position) != null ? Station.getItem(position).getIdStation() : Constants.ZERO_VALUE;
    }

    @Override
    public int getItemCount() {
        return Station.getSTATIONS().size();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public OnItemClickListener getOnItemClickListener() {
        return listener;
    }

}
