package com.cat.finalproject;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cat.finalproject.Database.DatabaseHandler;
import com.cat.finalproject.Detail.DetailController;
import com.cat.finalproject.Map.MapController;
import com.cat.finalproject.Objects.Station;
import com.cat.finalproject.PushNotification.GcmRegistrationAsyncTask;
import com.cat.finalproject.Utils.Constants;
import com.cat.finalproject.Utils.DialogClass;
import com.cat.finalproject.Utils.ParserXml;
import com.crashlytics.android.Crashlytics;
/*import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;*/
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import io.fabric.sdk.android.Fabric;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks{

    //Paypal Code
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;

    //Variables for Google+
    private ConnectionResult mConnectionResult;
    private boolean mIntentInProgress;
    private boolean mSignInClicked;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    private TextView emailT, username;
    private CircleImageView profilePic;

    DatabaseHandler db;

    // Facebook variables
//    private CallbackManager callbackManager;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(Constants.getConfigClientId())
                    // The following are only used in PayPalFuturePaymentActivity.
            .merchantName(Constants.getPaypalMerchantName())
            .merchantPrivacyPolicyUri(Uri.parse(Constants.getPaypalPrivacy()))
            .merchantUserAgreementUri(Uri.parse(Constants.getPaypalAgreement()));

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedState) {
        super.onRestoreInstanceState(savedState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fabric.with(this, new Crashlytics());
        new GcmRegistrationAsyncTask(this).execute();
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        callbackManager = CallbackManager.Factory.create();

        //Get a Tracker (should auto-report)
        ((AnalyticsHelper) getApplication()).getTracker(AnalyticsHelper.TrackerName.APP_TRACKER);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.mipmap.ic_launcher);
        setSupportActionBar(toolbar);

        setRefreshData();

        //Google+
        emailT = (TextView)findViewById(R.id.email);
        username = (TextView)findViewById(R.id.username);
        profilePic = (CircleImageView)findViewById(R.id.profile_image);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();

        //Initializing NavigationView
        navigationView = (NavigationView) findViewById(R.id.navigation_view);

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);

                //Closing drawer on item click
                drawerLayout.closeDrawers();

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.btn_sign_in:
                        Log.i(Constants.TAG_MAIN_ACTIVITY, String.valueOf(mSignInClicked));
                        if(!mSignInClicked) {
                            signInWithGplus();
                        }else{
                            signOutFromGplus();
                        }
                        return true;
                    case R.id.btn_facebook:
                        MenuItem faceButton = (MenuItem)findViewById(R.id.btn_facebook);
                        if(mSignInClicked) {
                            signOutFromGplus();


                            /*faceButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                                @Override
                                public void onSuccess(LoginResult loginResult) {
                                    String facebookMsg = "User ID:  " +
                                            loginResult.getAccessToken().getUserId() + "\n" +
                                            "Auth Token: " + loginResult.getAccessToken().getToken()
                                    Toast.makeText(getApplicationContext(), facebookMsg, Toast.LENGTH_SHORT).show();
                                }


                                @Override
                                public void onCancel() {
                                    Toast.makeText(getApplicationContext(), "Login attempt cancelled.", Toast.LENGTH_SHORT).show();
//                                info.setText("Login attempt cancelled.");
                                }

                                @Override
                                public void onError(FacebookException e) {
                                    Toast.makeText(getApplicationContext(), "Login attempt failed.", Toast.LENGTH_SHORT).show();
//                                info.setText("Login attempt failed.");
                                }
                            });*/
                        }
                        return true;
                    case R.id.about:
                        DialogClass dialog = new DialogClass(MainActivity.this);
                        dialog.showCustomDialog();
                        return true;
                    default:
                        Toast.makeText(getApplicationContext(),Constants.SOMETHING_WRONG,Toast.LENGTH_SHORT).show();
                        return true;
                }
            }
        });

        // Initializing Drawer Layout and ActionBarToggle
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();

        //Starts the load of the data
        new DownloadTaskXml().execute(Constants.URL);

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Executes this method i a separate Thread and starts to download the data
     */
    private class DownloadTaskXml extends AsyncTask<String, Void, List<Station>> {

        @Override
        protected List<Station> doInBackground(String... urls) {
            try {
                return parserXmlOfUrl(urls[0]);
            } catch (IOException e) {
                return null; // null if there is a network error
            } catch (XmlPullParserException e) {
                return null; // null if there is a parsing XML error
            }
        }


        /**
         * Updates the fragment with a map with markers
         *
         * @param result
         */
        @Override
        protected void onPostExecute(List<Station> result) {
            // Actualizar contenido del proveedor de datos
            Station.STATIONS = result;

            Fragment fragment = new MapController();
            String fragmentName = fragment.getClass().getName();
            Bundle b = new Bundle();
            b.putString("bikes", getResources().getString(R.string.bikes_available));
            b.putString("docks", getResources().getString(R.string.docks_available));

            fragment.setArguments(b);
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, fragment);
            fragmentTransaction.addToBackStack(fragmentName);
            fragmentTransaction.commit();
        }
    }

    /**
     * Retrieves the data from the XML
     *
     * @param urlString
     * @return
     * @throws XmlPullParserException
     * @throws IOException
     */
    private List<Station> parserXmlOfUrl(String urlString)
            throws XmlPullParserException, IOException {
        InputStream stream = null;
        ParserXml parserXml = new ParserXml();
        List<Station> entries = null;

        try {
            stream = downloadContent(urlString);
            entries = parserXml.parsear(stream);
        } finally {
            if (stream != null) {
                stream.close();
            }
        }
        return entries;
    }

    /**
     * Downloads the URL Content
     *
     * @param urlString
     * @return inputStream
     * @throws IOException
     */

    private InputStream downloadContent(String urlString) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(Constants.READ_TIMEOUT);
        conn.setConnectTimeout(Constants.CONNECT_TIMEOUT);
        conn.setRequestMethod(Constants.GET_VALUE);
        conn.setDoInput(true);
        // Init the request
        conn.connect();
        return conn.getInputStream();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }


    /**
     * Manages the option to execute based on the item Menu
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.show_list:
                // Get tracker.
                Tracker t = ((AnalyticsHelper) getApplication()).getTracker(AnalyticsHelper.TrackerName.APP_TRACKER);
                // Enable Advertising Features.
                t.enableAdvertisingIdCollection(true);
                // Build and Send the Analytics Event.
                HitBuilders.EventBuilder eventBuilder = new HitBuilders.EventBuilder();
                eventBuilder.setAction("Vote Me")
                        .setCategory("ToolBar")
                        .setValue(1L)
                        .setLabel("ShowList");
                t.send(eventBuilder.build());

                //Call the RecyclerView fragment
                Fragment fragment = new DetailController();
                String fragmentName = fragment.getClass().getName();
                android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, fragment);
                fragmentTransaction.addToBackStack(fragmentName);
                fragmentTransaction.commit();
                break;
            case R.id.donate:
                PayPalPayment thingToBuy = new PayPalPayment(Constants.ONE_DOLLAR,
                        Constants.DIVISION_DOLLAR,
                        Constants.COMPANY,
                        PayPalPayment.PAYMENT_INTENT_SALE);

                Intent intentPay = new Intent(MainActivity.this, PaymentActivity.class);
                intentPay.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
                startActivityForResult(intentPay, Constants.REQUEST_PAYPAL_PAYMENT);
                break;
            default:
                Log.e(Constants.TAG_MENU, Constants.NO_OPTION_ERROR);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Retrieves the result of the operation and calls the apropiate method
     *
     * @param requestCode
     * @param responseCode
     * @param intent
     */
    @Override
    protected void onActivityResult(int requestCode, int responseCode,
                                    Intent intent) {

        switch (requestCode) {
            case Constants.REQUEST_PAYPAL_PAYMENT:
                if (responseCode == Activity.RESULT_OK) {
                    PaymentConfirmation confirm = intent
                            .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                    if (confirm != null) {
                        try {
                            //System.out.println("Response " + confirm);
                            Log.i(Constants.TAG_PAYPAL, confirm.toJSONObject().toString());
                            JSONObject jsonObj = new JSONObject(confirm.toJSONObject().toString());

                            String paymentId = jsonObj.getJSONObject(Constants.getRESPONSE()).getString(Constants.getIDJSON());
                            Log.i(Constants.TAG_PAYPAL, "Payment id: " + paymentId);
                            Toast.makeText(getApplicationContext(), paymentId, Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            Log.e(Constants.TAG_PAYPAL, Constants.PAYPAL_ERROR, e);
                        }
                    }
                } else if (responseCode == Activity.RESULT_CANCELED) {
                    Log.i(Constants.TAG_PAYPAL, Constants.PAYPAL_USER_CANCEL);
                } else if (responseCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                    Log.i(Constants.TAG_PAYPAL, Constants.PAYPAL_INVALID_EXTRAS);
                }
                break;
            case Constants.REQUEST_GOOGLE://R.id.btn_sign_in:
                Log.i(Constants.TAG_MAIN_ACTIVITY, "onActivityResult");
                if (requestCode == Constants.REQUEST_GOOGLE) {
                    if (responseCode != RESULT_OK) {
                        mSignInClicked = false;
                    }

                    mIntentInProgress = false;

                    if (!mGoogleApiClient.isConnecting()) {
                        mGoogleApiClient.connect();
                    }
                }
                break;
            case Constants.REQUEST_FACEBOOK:
//                callbackManager.onActivityResult(requestCode, responseCode, intent);
//                Session.getActiveSession().onActivityResult(this, requestCode, responseCode, intent);
                break;
            default:
                Log.e(Constants.TAG_MAIN_ACTIVITY, Constants.NO_REQUEST_CODE_FOUND);
                break;
        }
    }

    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    /**
     * Method to resolve any signin errors
     * */
    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, Constants.REQUEST_GOOGLE);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
                Log.e(Constants.TAG_MAIN_ACTIVITY, "connectedInResolution");
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
                    Constants.ZERO_VALUE).show();
            return;
        }

        if (!mIntentInProgress) {
            // Store the ConnectionResult for later usage
            mConnectionResult = result;

            if (mSignInClicked) {
                // The user has already clicked 'sign-in' so we attempt to
                // resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        Log.i(Constants.TAG_MAIN_ACTIVITY, "onConnectionSuspended");
        mGoogleApiClient.connect();
        updateUI(false);
    }

    @Override
    public void onConnected(Bundle arg0) {
        Log.i(Constants.TAG_MAIN_ACTIVITY, "onConnected");
        mSignInClicked = false;
        Toast.makeText(this, R.string.user_connected, Toast.LENGTH_LONG).show();

        // Get user's information
        getProfileInformation();

        // Update the UI after signin
        updateUI(true);
    }

    /**
     * Updating the UI, showing/hiding buttons and profile layout
     * */
    private void updateUI(boolean isSignedIn) {
        if (isSignedIn) {
            mSignInClicked = true;
        } else {
            mSignInClicked = false;
        }
    }

    /**
     * Sign-in into google
     * */
    private void signInWithGplus() {
        if (!mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            resolveSignInError();
        }
    }

    /**
     * Sign-out from google
     * */
    private void signOutFromGplus() {
        Log.i(Constants.TAG_MAIN_ACTIVITY, "signOutGoogle");
        if (mGoogleApiClient.isConnected()) {
            Log.i(Constants.TAG_MAIN_ACTIVITY, "signOutGoogleIF");
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            setDefaultProfileInformation();
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
            updateUI(false);
        }
    }

    /**
     * Fetching user's information name, email, profile pic
     * */
    private void setDefaultProfileInformation() {
        Log.i(Constants.TAG_MAIN_ACTIVITY, "setDefaultInfo");
        try {
            username.setText(R.string.your_name);
            emailT.setText(R.string.your_email);

            // by default the profile url gives 50x50 px image only
            // we can replace the value with whatever dimension we want by
            // replacing sz=X
            profilePic.setImageResource(R.drawable.profile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Fetching user's information name, email, profile pic
     * */
    private void getProfileInformation() {
        Log.i(Constants.TAG_MAIN_ACTIVITY, "getProfileInfo");
        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);
                String personName = currentPerson.getDisplayName();
                String personPhotoUrl = currentPerson.getImage().getUrl();
                String personGooglePlusProfile = currentPerson.getUrl();
                String email = Plus.AccountApi.getAccountName(mGoogleApiClient);

                Log.e(Constants.TAG_MAIN_ACTIVITY, "Name: " + personName + ", plusProfile: "
                        + personGooglePlusProfile + ", email: " + email
                        + ", Image: " + personPhotoUrl);

                username.setText(personName);
                emailT.setText(email);

                // by default the profile url gives 50x50 px image only
                // we can replace the value with whatever dimension we want by
                // replacing sz=X

                new LoadProfileImage(profilePic).execute(personPhotoUrl);

            } else {
                Toast.makeText(getApplicationContext(),
                        "Person information is null", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Background Async task to load user profile picture from url
     * */
    private class LoadProfileImage extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public LoadProfileImage(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e(Constants.TAG_MAIN_ACTIVITY, e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Creating Database with only one register
     */
    public void setRefreshData() {
        db = new DatabaseHandler(this);
        //db.getWritableDatabase();
        db.AddLocation();
        db.close();
    }

    private void facebookSignIn(){
/*
        Session.openActiveSession(getActivity(), true, new Session.StatusCallback() {
            @Override
            public void call(Session session, SessionState state, Exception exception) {
                if (exception != null) {
                    Log.d("Facebook", exception.getMessage());
                }
                Log.d("Facebook", "Session State: " + session.getState());
                // you can make request to the /me API or do other stuff like post, etc. here
            }

        });
*/
    /*    Session.openActiveSession(this, true, new Session.StatusCallback() {

            // callback when session changes state
            @Override
            public void call(Session session, SessionState state, Exception exception) {
                if (session.isOpened()) {

                    // make request to the /me API
                    Request.newMeRequest(session, new Request.GraphUserCallback() {

                        // callback after Graph API response with user object
                        @Override
                        public void onCompleted(GraphUser user, Response response) {
                            if (user != null) {
                                TextView welcome = (TextView) findViewById(R.id.welcome);
                                welcome.setText("Hello " + user.getName() + "!");
                            }
                        }
                    }).executeAsync();
                }
            }
        });*/
    }

}
