package com.cat.finalproject.Utils;

import java.math.BigDecimal;

/**
 * Created by Exchange on 8/26/2015.
 */
public class Constants {


    // Constants of XML Response
    public static final String TAG_STATIONS = "stations";
    public static final String TAG_STATION = "station";
    public static final String TAG_ID_STATION = "id";
    public static final String TAG_NAME = "name";
    public static final String TAG_TERMINAL_NAME = "terminalName";
    public static final String TAG_LAT = "lat";
    public static final String TAG_LONG = "long";
    public static final String TAG_INSTALLED = "installed";
    public static final String TAG_LOCKED = "locked";
    public static final String TAG_INSTALL_DATE = "installDate";
    public static final String TAG_REMOVAL_DATE = "removalDate";
    public static final String TAG_TEMPORARY = "temporary";
    public static final String TAG_NB_BIKES = "nbBikes";
    public static final String TAG_NB_EMPTY_DOCKS = "nbEmptyDocks";
    public static final String TAG_NB_DOCKS = "nbDocks";

    // Constants for work
    public final static int READ_TIMEOUT = 10000;
    public final static int CONNECT_TIMEOUT = 15000;
    public final static String URL = "https://tfl.gov.uk/tfl/syndication/feeds/cycle-hire/livecyclehireupdates.xml";
    private static final String CONFIG_CLIENT_ID = "ASmfl3K2JlvL6-QJCwGDaDRTomD3AoX7i_B1V02KTD_JN0rcg8CKzoUf-qc_bapp9off2_Boe91jWPQF";
    public static final String TAG_EXTRA_INTENT = "IdElement";
    public static final String URL_GOOGLE_PLUS_BUTTON = "https://tfl.gov.uk/tfl/syndication/feeds/cycle-hire/livecyclehireupdates.xml";
    private static final String PROPERTY_ID = "UA-66275501-2";

    // Tags for Log
    public static final String TAG_PARSER_XML = "ParserXml";
    public static final String TAG_MAP_CONTROLLER = "MapController";
    public static final String TAG_PAYPAL = "Paypal";
    public static final String TAG_MENU = "Menu";
    public static final String TAG_ADAPTER = "Adapter";
    public static final String TAG_DETAIL_CONTROLLER = "DetailController";
    public static final String TAG_MAP_ROUTING = "MapRouting";
    public static final String TAG_MAIN_ACTIVITY = "MainActivity";
    public static final String TAG_ANALYTICS_HELPER = "AnalyticsHelper";
    public static final String TAG_DATABASE_HANDLER = "DatabaseHandler";


    // Generic Values
    public static final String TRUE_VALUE = "true";
    public static final String GET_VALUE = "GET";
    public static final int ZOOM_LEVEL = 15;
    public static final int REQUEST_PAYPAL_PAYMENT = 2;
    public static final BigDecimal ONE_DOLLAR = new BigDecimal(1);
    public static final String DIVISION_DOLLAR = "USD";
    public static final String COMPANY = "theappexperts.co.uk";
    private static final String RESPONSE = "response";
    private static final String IDJSON = "id";
    private static final String PAYPAL_PRIVACY = "https://www.example.com/privacy";
    private static final String PAYPAL_AGREEMENT = "https://www.example.com/legal";
    private static final String PAYPAL_MERCHANT_NAME = "The Appexperts Ltd";
    public static final int ZERO_VALUE = 0;
    public static final String TAG_LATITUDE = "destinationLat";
    public static final String TAG_LONGITUDE = "destinationLng";
    public static final int REQUEST_GOOGLE = 0;
    public static final int REQUEST_FACEBOOK = 3;
    public static final int ONE_VALUE = 1;

    //Errors
    public static final String PAYPAL_ERROR = "an extremely unlikely failure occurred: ";
    public static final String PAYPAL_USER_CANCEL = "The user canceled.";
    public static final String PAYPAL_INVALID_EXTRAS = "An invalid Payment was submitted. Please see the docs.";
    public static final String NO_OPTION_ERROR = "Option Unknown";
    public static final String NO_STATION_ERROR = "There are not stations with ID = ";
    public static final String UNABLE_MAPS = "Sorry! unable to create maps";
    public static final String ROUTING_FAILURE = "Something went wrong, Try again";
    public static final String NO_ID_VIEW_FOUND = "No Id Found";
    public static final String SOMETHING_WRONG = "Somethings Wrong";
    public static final String NO_REQUEST_CODE_FOUND = "Not Request Code Found";

    public static String getPropertyId() {
        return PROPERTY_ID;
    }

    public static String getConfigClientId() {
        return CONFIG_CLIENT_ID;
    }

    public static String getRESPONSE() {
        return RESPONSE;
    }

    public static String getIDJSON() {
        return IDJSON;
    }

    public static String getPaypalPrivacy() {
        return PAYPAL_PRIVACY;
    }

    public static String getPaypalAgreement() {
        return PAYPAL_AGREEMENT;
    }

    public static String getPaypalMerchantName() {
        return PAYPAL_MERCHANT_NAME;
    }
}
