package com.cat.finalproject.Map;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cat.finalproject.Objects.Station;
import com.cat.finalproject.R;
import com.cat.finalproject.Utils.Constants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

/**
 * Created by Exchange on 8/27/2015.
 */
public class MapController extends Fragment {

    private GoogleMap mMap;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mapView = inflater.inflate(R.layout.map_fragment, container, false);

        try{
            setUpMapIfNeeded();
        }catch(Exception e){
            Log.e(Constants.TAG_MAP_CONTROLLER, e.getMessage());
        }

        return mapView;
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        // Changing map type
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        // Showing / hiding your current location
        //mMap.setMyLocationEnabled(true);

        // Enable / Disable zooming controls
        mMap.getUiSettings().setZoomControlsEnabled(false);

        // Enable / Disable my location button
        //mMap.getUiSettings().setMyLocationButtonEnabled(true);

        // Enable / Disable Compass icon
        mMap.getUiSettings().setCompassEnabled(true);

        // Enable / Disable Rotate gesture
        mMap.getUiSettings().setRotateGesturesEnabled(true);

        // Enable / Disable zooming functionality
        mMap.getUiSettings().setZoomGesturesEnabled(true);

        setMarkers();
    }


    /**
     * Set the markeson the map based in the list of places retrieved on the XML
     */
    private void setMarkers(){
        try {
            List<Station> location_list = Station.getSTATIONS();

            Log.i(Constants.TAG_MAP_CONTROLLER, String.valueOf(location_list.size()));

            Double lastLat = new Double(0.0);
            Double lastLng = new Double(0.0);

            for (int i = 0; i < location_list.size(); i++) {

                MarkerOptions marker = new MarkerOptions().position(
                        new LatLng(
                                new Double (location_list.get(i).getLat()),
                                new Double(location_list.get(i).getLng())))
                        .title(location_list.get(i).getName())
                        .snippet(getArguments().getString("bikes") + " " + location_list.get(i).getNbBikes() +
                                " / " + getArguments().getString("docks") + " " + location_list.get(i).getNbEmptyDocks())
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

                mMap.addMarker(marker);

                if(i == (location_list.size()-1)){
                    lastLat = new Double(location_list.get(i).getLat());
                    lastLng = new Double(location_list.get(i).getLng());
                }
            }

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(lastLat, lastLng))
                    .zoom(Constants.ZOOM_LEVEL).build();
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }catch(Exception e){
            Log.e(Constants.TAG_MAP_CONTROLLER, e.getMessage());
        }
    }
}
