package com.cat.finalproject.Map;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cat.finalproject.Database.DatabaseHandler;
import com.cat.finalproject.Objects.Location;
import com.cat.finalproject.Objects.Station;
import com.cat.finalproject.R;
import com.cat.finalproject.RouteLib.AbstractRouting;
import com.cat.finalproject.RouteLib.Route;
import com.cat.finalproject.RouteLib.Routing;
import com.cat.finalproject.RouteLib.RoutingListener;
import com.cat.finalproject.Utils.Constants;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

/**
 * Created by Exchange on 8/28/2015.
 */
public class MapRouting extends Fragment implements RoutingListener {

    private LatLng start;// = new LatLng(51.52916347,-0.109970527);
    private LatLng end;
    //private ProgressDialog progressDialog;
    private Polyline polyline;
    protected GoogleMap map;
    private String destLan;
    private String destLng;

    private String startLat;
    private String startLon;

    DatabaseHandler db;

    /**
     * CONSTRUCTOR
     */
    public MapRouting() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.map_fragment, container, false);

        initilizeMap();
        db = new DatabaseHandler(getActivity());
        Location location = db.getLocation(Constants.ONE_VALUE);
        db.close();
        start = new LatLng(Double.valueOf(location.getLatitude()), Double.valueOf(location.getLongitude()));

        if(getArguments() != null) {
            destLan = getArguments().getString(Constants.TAG_LATITUDE).toString();
            destLng = getArguments().getString(Constants.TAG_LONGITUDE).toString();

            end = new LatLng(Double.parseDouble(destLan), Double.parseDouble(destLng));
            route(end);
        }

        return view;
    }

    /**
     * function to load map If map is not created it will create it for you
     */
    private void initilizeMap() {
        if (map == null) {
            map = ((SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map)).getMap();
            // check if map is created successfully or not
            if (map == null) {
                Log.e(Constants.TAG_MAP_ROUTING, Constants.UNABLE_MAPS);
            }
        }

        // Enable / Disable zooming controls
        map.getUiSettings().setZoomControlsEnabled(false);
        // Enable / Disable zooming functionality
        map.getUiSettings().setZoomGesturesEnabled(true);
    }


    public void route(LatLng dest){

        if(end == null) {
            end = dest;
        }else{
//            progressDialog = ProgressDialog.show(getActivity().getApplicationContext(), "Please wait.",
//                    "Fetching route information.", true);
            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(this)
                    .waypoints(start, end)
                    .build();
            routing.execute();
        }
    }


    @Override
    public void onRoutingFailure() {
        // The Routing request failed
        //progressDialog.dismiss();
        Toast.makeText(getActivity().getApplicationContext(),Constants.ROUTING_FAILURE, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRoutingStart() {
        // The Routing Request starts
    }

    @Override
    public void onRoutingSuccess(PolylineOptions mPolyOptions, Route route) {
        //progressDialog.dismiss();
        CameraUpdate center = CameraUpdateFactory.newLatLng(start);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(Constants.ZOOM_LEVEL);

        map.moveCamera(center);

        if(polyline != null)
            polyline.remove();

        polyline = null;
        //adds route to the map.
        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.color(getResources().getColor(R.color.primary_dark));
        polyOptions.width(10);
        polyOptions.addAll(mPolyOptions.getPoints());
        polyline = map.addPolyline(polyOptions);

        // Start marker
        MarkerOptions options = new MarkerOptions();
        options.position(start);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.start_blue));
        map.addMarker(options);

        // End marker
        options = new MarkerOptions();
        options.position(end);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.end_green));
        map.addMarker(options);

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(start)
                .zoom(Constants.ZOOM_LEVEL).build();
        map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void onRoutingCancelled() {
        Log.i(Constants.TAG_MAP_ROUTING, "Routing was cancelled.");
    }


}
