package com.cat.finalproject.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.cat.finalproject.Objects.Location;
import com.cat.finalproject.Utils.Constants;

import java.util.ArrayList;

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "finalproject.db";

    // Contacts table name
    private static final String TABLE_LOCATION = "location";

    // Contacts Table Columns names
    private static final String KEY_ID = "_id";
    private static final String KEY_LATITUDE = "latitude";
    private static final String KEY_LONGITUDE = "longitude";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(Constants.TAG_DATABASE_HANDLER, "Database ");
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_LOCATION + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_LATITUDE + " TEXT,"
                + KEY_LONGITUDE + " TEXT" + ");";
        db.execSQL(CREATE_CONTACTS_TABLE);
        Log.i(Constants.TAG_DATABASE_HANDLER, "Database created");
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION);

        // Create tables again
        onCreate(db);
    }

   /* public DatabaseHandler open() throws SQLException
    {
        db = DatabaseHandler.getWritableDatabase();
        return this;
    }*/

    /**
     *  Operations
     */


    // Getting location
    public Location getLocation(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_LOCATION, new String[]{KEY_ID,
                        KEY_LATITUDE, KEY_LONGITUDE}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Location location = new Location(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));

        Log.i(Constants.TAG_DATABASE_HANDLER, "lat " + location.getLatitude() + " lng " + location.getLongitude());
        cursor.close();
        db.close();

        return location;
    }

    // Adding new location
    public void AddLocation() {
        Log.i(Constants.TAG_DATABASE_HANDLER, "Database Add Location");
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_LATITUDE, "51.521198");
        values.put(KEY_LONGITUDE, "-0.115157");
        // Inserting Row
        db.insert(TABLE_LOCATION, null, values);
        db.close(); // Closing database connection
    }

}
