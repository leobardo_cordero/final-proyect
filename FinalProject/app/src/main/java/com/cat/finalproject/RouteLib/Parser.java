package com.cat.finalproject.RouteLib;

//. by Haseem Saheed
public interface Parser {
    public Route parse();
}