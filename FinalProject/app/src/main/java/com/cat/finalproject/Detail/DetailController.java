package com.cat.finalproject.Detail;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.cat.finalproject.Map.MapRouting;
import com.cat.finalproject.Objects.Station;
import com.cat.finalproject.R;
import com.cat.finalproject.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import com.cat.finalproject.FabLib.codetail.fabtoolbar.FabToolbar;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Manages the RecyclerView
 *
 * Created by Exchange on 8/27/2015.
 */
public class DetailController extends Fragment implements DetailAdapter.OnItemClickListener,
        SearchView.OnQueryTextListener{

    @InjectView(R.id.recycler)
    RecyclerView recycler;

    private DetailAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<Station> listStations;
    private Bundle globalData;
    private SearchView searchView;
    //private Station station;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View recyclerView = inflater.inflate(R.layout.detail_fragment, container, false);

        ButterKnife.inject(this,recyclerView);
        setHasOptionsMenu(true);

        recycler.hasFixedSize();
        recycler.setItemAnimator(new DefaultItemAnimator());

        layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recycler.setLayoutManager(layoutManager);
        //Log.i(Constants.TAG_DETAIL_CONTROLLER, Station.getSTATIONS() != null ? String.valueOf(Station.getSTATIONS().size()) : "");

        listStations = Station.getSTATIONS();

        adapter = new DetailAdapter(listStations, getActivity().getApplicationContext());
        adapter.setHasStableIds(true);
        adapter.setOnItemClickListener(this);
        recycler.setAdapter(adapter);

        return recyclerView;
    }

    /**
     * Calls a new fragment for showing navigation route since the current location
     *
     * @param item
     * @param position
     */
    @Override
    public void onItemClick(DetailAdapter.ViewHolder item, int position) {
        Fragment fragment = new MapRouting();
        String fragmentName = fragment.getClass().getName();
        String destLan = listStations.get(position).getLat().toString();
        String destLng = listStations.get(position).getLng().toString();

        globalData = new Bundle();
        globalData.putString(Constants.TAG_LATITUDE, destLan);
        globalData.putString(Constants.TAG_LONGITUDE, destLng);

        fragment.setArguments(globalData);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.addToBackStack(fragmentName);

        fragmentTransaction.commit();
    }


    /**
     * Inflates the fragment with a new menu
     *
     * @param menu
     * @param inflater
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_filter, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextChange(String query) {
        final List<Station> filteredModelList = filter(adapter.getItemsOriginal(), query);
//        final List<Station> filteredModelList = filter(listStations, query);
//        final List<Station> filteredModelList = filter(station.STATIONS, query);
        Log.i(Constants.TAG_DETAIL_CONTROLLER, String.valueOf(listStations.size()));
        adapter.animateTo(filteredModelList);
        recycler.scrollToPosition(Constants.ZERO_VALUE);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    /**
     * Filther the main list based on the typing and returns the filtered list
     *
     * @param models
     * @param query
     * @return filteredModelList
     */
    private List<Station> filter(List<Station> models, String query) {
        query = query.toLowerCase();

        Log.i(Constants.TAG_DETAIL_CONTROLLER, "Entro al filther");
        final List<Station> filteredModelList = new ArrayList<>();

        for (Station model : models) {
            final String text = model.getName().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }
}