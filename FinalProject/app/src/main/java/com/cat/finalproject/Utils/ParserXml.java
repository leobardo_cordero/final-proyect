package com.cat.finalproject.Utils;

import android.util.Xml;

import com.cat.finalproject.Objects.Station;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Exchange on 8/26/2015.
 *
 * This Class manages the Conversion from XML to Java Objects
 */
public class ParserXml {

    // Namespace general. null if not exists
    private static final String ns = null;


    /**
     * Parse a XML inputStream to an object list
     *
     * @param in inputStream
     * @return List of Station
     * @throws XmlPullParserException
     * @throws IOException
     */
    public List<Station> parsear(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, true);
            parser.setInput(in, null);
            parser.nextTag();
            return readStations(parser);
        } finally {
            in.close();
        }
    }

    /**
     * Converts a series of tags <station> into a list
     *
     * @param parser
     * @return list of Stations
     * @throws XmlPullParserException
     * @throws IOException
     */
    private List<Station> readStations(XmlPullParser parser)
            throws XmlPullParserException, IOException {
        List<Station> listStations = new ArrayList<Station>();

        parser.require(XmlPullParser.START_TAG, ns, Constants.TAG_STATIONS);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String nameTag = parser.getName();
            // Search for Tag <station>
            if (nameTag.equals(Constants.TAG_STATION)) {
                listStations.add(readStation(parser));
            } else {
                skipTag(parser);
            }
        }

        return listStations;
    }

    /**
     * Converts a tag <station> in a Station object
     *
     * @param parser parser XML
     * @return new Station object
     * @throws XmlPullParserException
     * @throws IOException
     */
    private Station readStation(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, Constants.TAG_STATION);
        int idStation = 0;
        String name = null;
        String terminalName = null;
        Double lat = new Double(0.0);
        Double lng = new Double(0.0);
        boolean installed = false;
        boolean locked = false;
        String installDate = null;
        String removalDate = null;
        boolean temporary = false;
        int nbBikes = 0;
        int nbEmptyDocks = 0;
        int nbDocks = 0;


        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String nameTag = parser.getName();

            switch (nameTag) {
                case Constants.TAG_ID_STATION:
                    idStation = readIdStation(parser);
                    break;
                case Constants.TAG_NAME:
                    name = readName(parser);
                    break;
                case Constants.TAG_TERMINAL_NAME:
                    terminalName = readTerminalName(parser);
                    break;
                case Constants.TAG_LAT:
                    lat = readLat(parser);
                    break;
                case Constants.TAG_LONG:
                    lng = readLng(parser);
                    break;
                case Constants.TAG_INSTALLED:
                    installed = readInstalled(parser);
                    break;
                case Constants.TAG_LOCKED:
                    locked = readLocked(parser);
                    break;
                case Constants.TAG_INSTALL_DATE:
                    installDate = readInstallDate(parser);
                    break;
                case Constants.TAG_REMOVAL_DATE:
                    removalDate = readRemovalDate(parser);
                    break;
                case Constants.TAG_TEMPORARY:
                    temporary = readTemporary(parser);
                    break;
                case Constants.TAG_NB_BIKES:
                    nbBikes = readNbBikes(parser);
                    break;
                case Constants.TAG_NB_EMPTY_DOCKS:
                    nbEmptyDocks = readEmptyDocks(parser);
                    break;
                case Constants.TAG_NB_DOCKS:
                    nbDocks = readNbDocks(parser);
                    break;
                default:
                    skipTag(parser);
                    break;
            }
        }

        return new Station(
                idStation,
                name,
                terminalName,
                lat,
                lng,
                installed,
                locked,
                installDate,
                removalDate,
                temporary,
                nbBikes,
                nbEmptyDocks,
                nbDocks
        );
    }

    // Process tag <idStation>
    private int readIdStation(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, Constants.TAG_ID_STATION);
        int stationId = Integer.parseInt(getText(parser));
        parser.require(XmlPullParser.END_TAG, ns, Constants.TAG_ID_STATION);
        return stationId;
    }

    // Process tag <name>
    private String readName(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, Constants.TAG_NAME);
        String tagName = getText(parser);
        parser.require(XmlPullParser.END_TAG, ns, Constants.TAG_NAME);
        return tagName;
    }

    // Process tag <terminalName>
    private String readTerminalName(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, Constants.TAG_TERMINAL_NAME);
        String nameTerminal = getText(parser);
        parser.require(XmlPullParser.END_TAG, ns, Constants.TAG_TERMINAL_NAME);
        return nameTerminal;
    }

    // Process tag <lat>
    private Double readLat(XmlPullParser parser)
            throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, Constants.TAG_LAT);
        Double latitud = new Double(getText(parser));
        parser.require(XmlPullParser.END_TAG, ns, Constants.TAG_LAT);
        return latitud;
    }

    // Process tag <long>
    private Double readLng(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, Constants.TAG_LONG);
        Double longitude = new Double(getText(parser));
        parser.require(XmlPullParser.END_TAG, ns, Constants.TAG_LONG);
        return longitude;
    }

    // Process tag <installed>
    private boolean readInstalled(XmlPullParser parser) throws IOException, XmlPullParserException {
        String description = "";
        boolean install = false;
        parser.require(XmlPullParser.START_TAG, ns, Constants.TAG_INSTALLED);
        description = getText(parser);

        if(Constants.TRUE_VALUE.equals(description)){
            install = true;
        }
        parser.require(XmlPullParser.END_TAG, ns, Constants.TAG_INSTALLED);
        return install;
    }

    // Process tag <locked>
    private boolean readLocked(XmlPullParser parser) throws IOException, XmlPullParserException {
        String description = "";
        boolean lockedTag = false;
        parser.require(XmlPullParser.START_TAG, ns, Constants.TAG_LOCKED);
        description = getText(parser);

        if(Constants.TRUE_VALUE.equals(description)){
            lockedTag = true;
        }

        parser.require(XmlPullParser.END_TAG, ns, Constants.TAG_LOCKED);
        return lockedTag;
    }

    // Process tag <installDate>
    private String readInstallDate(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, Constants.TAG_INSTALL_DATE);
        String insDate = getText(parser);
        parser.require(XmlPullParser.END_TAG, ns, Constants.TAG_INSTALL_DATE);
        return insDate;
    }

    // Process tag <removalDate>
    private String readRemovalDate(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, Constants.TAG_REMOVAL_DATE);
        String remDate = getText(parser);
        parser.require(XmlPullParser.END_TAG, ns, Constants.TAG_REMOVAL_DATE);
        return remDate;
    }

    // Process tag <temporary>
    private boolean readTemporary(XmlPullParser parser) throws IOException, XmlPullParserException {
        String tempTag = "";
        boolean temp = false;
        parser.require(XmlPullParser.START_TAG, ns, Constants.TAG_TEMPORARY);
        tempTag = getText(parser);
        if(Constants.TRUE_VALUE.equals(tempTag)){
            temp = true;
        }
        parser.require(XmlPullParser.END_TAG, ns, Constants.TAG_TEMPORARY);
        return temp;
    }

    // Process tag <nbbikes>
    private int readNbBikes(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, Constants.TAG_NB_BIKES);
        int bikesTag = Integer.parseInt(getText(parser));
        parser.require(XmlPullParser.END_TAG, ns, Constants.TAG_NB_BIKES);
        return bikesTag;
    }

    // Process tag <nbemptydocks>
    private int readEmptyDocks(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, Constants.TAG_NB_EMPTY_DOCKS);
        int empDocks = Integer.parseInt(getText(parser));
        parser.require(XmlPullParser.END_TAG, ns, Constants.TAG_NB_EMPTY_DOCKS);
        return empDocks;
    }

    // Process tag <nbdocks>
    private int readNbDocks(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, Constants.TAG_NB_DOCKS);
        int docks = Integer.parseInt(getText(parser));
        parser.require(XmlPullParser.END_TAG, ns, Constants.TAG_NB_DOCKS);
        return docks;
    }

    // Retrieves the text from atributes
    private String getText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    // Skip the object that are not needed
    private void skipTag(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }

        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}