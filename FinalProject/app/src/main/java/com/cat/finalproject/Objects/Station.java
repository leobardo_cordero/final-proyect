package com.cat.finalproject.Objects;

import android.util.Log;

import com.cat.finalproject.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Exchange on 8/26/2015.
 *
 *Definition of the Object
 */
public class Station {

    private int idStation;
    private String name;
    private String terminalName;
    private Double lat;
    private Double lng;
    private boolean installed;
    private boolean locked;
    private String installDate;
    private String removalDate;
    private boolean temporary;
    private int nbBikes;
    private int nbEmptyDocks;
    private int nbDocks;


    // Proveedor estático de datos para el adaptador
    public static List<Station> STATIONS = new ArrayList<>();

    /**
     * Retrieves All the Stations
     *
     * @return List<STATIONS>
     */
    public static List<Station> getSTATIONS(){
        return STATIONS;
    }

    /**
     *Retrieves an object from the list of Stations
     *
     * @param id
     * @return Station
     */
    public static Station getItem(int id) {
        for (Station item : STATIONS) {
            if (item.getIdStation() == id) {
                return item;
            }
        }
        return null;
    }

    /**
     * Returns ths List of Stations Names
     *
     */

    public static String[] getListStationNames(){
        List<Station> totalStations = getSTATIONS();
        String[] myArray = new String[totalStations.size()];

        for(int i = 0 ; i < totalStations.size() ; i++){
            myArray[i] = totalStations.get(i).getName();
        }

        return myArray;
    }

    /**
     * Constructor
     *
     * @param idStation
     * @param name
     * @param terminalName
     * @param lat
     * @param lng
     * @param installed
     * @param locked
     * @param installDate
     * @param removalDate
     * @param temporary
     * @param nbBikes
     * @param nbEmptyDocks
     * @param nbDocks
     */
    public Station(int idStation, String name, String terminalName, Double lat, Double lng, boolean installed, boolean locked, String installDate, String removalDate, boolean temporary, int nbBikes, int nbEmptyDocks, int nbDocks) {
        this.idStation = idStation;
        this.name = name;
        this.terminalName = terminalName;
        this.lat = lat;
        this.lng = lng;
        this.installed = installed;
        this.locked = locked;
        this.installDate = installDate;
        this.removalDate = removalDate;
        this.temporary = temporary;
        this.nbBikes = nbBikes;
        this.nbEmptyDocks = nbEmptyDocks;
        this.nbDocks = nbDocks;
    }

    /**
     *
     * Getters & Setters
     */

    public int getIdStation() {
        return idStation;
    }

    public void setIdStation(int idStation) {
        this.idStation = idStation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTerminalName() {
        return terminalName;
    }

    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public boolean isInstalled() {
        return installed;
    }

    public void setInstalled(boolean installed) {
        this.installed = installed;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public String getInstallDate() {
        return installDate;
    }

    public void setInstallDate(String installDate) {
        this.installDate = installDate;
    }

    public String getRemovalDate() {
        return removalDate;
    }

    public void setRemovalDate(String removalDate) {
        this.removalDate = removalDate;
    }

    public boolean isTemporary() {
        return temporary;
    }

    public void setTemporary(boolean temporary) {
        this.temporary = temporary;
    }

    public int getNbBikes() {
        return nbBikes;
    }

    public void setNbBikes(int nbBikes) {
        this.nbBikes = nbBikes;
    }

    public int getNbEmptyDocks() {
        return nbEmptyDocks;
    }

    public void setNbEmptyDocks(int nbEmptyDocks) {
        this.nbEmptyDocks = nbEmptyDocks;
    }

    public int getNbDocks() {
        return nbDocks;
    }

    public void setNbDocks(int nbDocks) {
        this.nbDocks = nbDocks;
    }
}
