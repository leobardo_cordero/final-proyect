package com.cat.finalproject;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.robotium.solo.Solo;

import java.util.ArrayList;

/**
 * Created by Exchange on 9/5/2015.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {
    private Solo solo;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        solo = new Solo(getInstrumentation(), getActivity());
    }


    public void testListMarkers() {
        try {
            solo.assertCurrentActivity("wrong activity", MainActivity.class);
            solo.sleep(10000);
            solo.clickOnMenuItem("Show List");
            solo.sleep(5000);
            //solo.clickInRecyclerView(R.id.fab_toolbar, 5);
            ArrayList<View> list = solo.getCurrentViews();
            Log.i("leo", String.valueOf(list.size()));

//            ArrayList<View> list = solo.getCurrentViews();
            for (View v : list) {
                if (v.getId() != View.NO_ID) {
                    String id = v.getResources().getResourceName(v.getId()).replaceAll(":id/", ".R.id.");
                    Log.i("DEBUG", id);
                }
            }

            solo.sleep(12000);
//            solo.getView();




           /* 27
            mChildren
            0
            mChildren
            4
            mMatchParentChildren
            0
            button
*/
            solo.clickOnButton(34);
            solo.sleep(15000);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void tearDown() throws Exception{

        solo.finishOpenedActivities();
    }

}
